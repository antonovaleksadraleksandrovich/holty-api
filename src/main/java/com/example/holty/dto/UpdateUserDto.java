package com.example.holty.dto;

import lombok.Data;

public @Data
class UpdateUserDto {
    private Integer userId;
    private UserDto user;
}

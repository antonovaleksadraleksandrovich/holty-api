package com.example.holty.dto;

import lombok.Data;

public @Data class AddRecordDto {
    private Integer userId;
    private RecordDto record;
}

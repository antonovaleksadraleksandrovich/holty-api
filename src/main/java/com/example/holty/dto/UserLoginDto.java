package com.example.holty.dto;

import lombok.Data;

public @Data class UserLoginDto {
    private String login;
    private String password;

}

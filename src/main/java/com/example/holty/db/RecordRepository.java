package com.example.holty.db;

import com.example.holty.model.RecordEntry;
import com.example.holty.model.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RecordRepository extends JpaRepository<RecordEntry, Integer> {
    public Iterable<RecordEntry> findByUser(User user);
}

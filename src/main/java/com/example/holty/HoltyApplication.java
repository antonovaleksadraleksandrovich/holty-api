package com.example.holty;
import com.example.holty.db.RecordRepository;
import com.example.holty.db.UserRepository;
import com.example.holty.dto.AddRecordDto;
import com.example.holty.dto.UpdateUserDto;
import com.example.holty.dto.UserDto;
import com.example.holty.dto.UserLoginDto;
import com.example.holty.model.RecordEntry;
import com.example.holty.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;
import java.util.UUID;

@SpringBootApplication
@RestController
public class HoltyApplication {
    @Autowired
    RecordRepository recordRepository;

    @Autowired
    UserRepository userRepository;

    public static void main(String[] args) {
        SpringApplication.run(HoltyApplication.class, args);
    }

    @GetMapping("/users/rnd")
    public User addRndRecord() {
        User user = new User();
        user.setBirthday(UUID.randomUUID().toString());
        user.setEmail(UUID.randomUUID().toString());
        user.setMedicalId(UUID.randomUUID().toString());
        user.setGestationDate(UUID.randomUUID().toString());
        user.setLogin("doodoo");
        user.setPassword("12345");

        userRepository.save(user);
        User entity = userRepository.findAll().get(0);
        return entity;
    }

    @PostMapping("/records")
    public Iterable<RecordEntry> getAllRecords(@RequestParam Integer userId) {
        Optional<User> user = userRepository.findById(userId);
        if (user.isPresent()) {
            return recordRepository.findByUser(user.get());
        }
        throw new RuntimeException("user not found or something");
    }

    @GetMapping("/users")
    public Iterable<User> getAllUsers() {
        return userRepository.findAll();
    }

    @PostMapping("/records/add")
    public RecordEntry addRecord(@RequestBody AddRecordDto record) {
        Optional<User> user = userRepository.findById(record.getUserId());
        if (user.isPresent()) {
            RecordEntry entry = new RecordEntry();
            entry.setUser(user.get());
            entry.setData(record.getRecord().getData());
            entry.setDate(record.getRecord().getDate());
            entry.setArterialPressure(record.getRecord().getArterialPressure());
            entry.setResult(record.getRecord().getResult());
            entry.setStatus(record.getRecord().getStatus());
            entry.setTime(record.getRecord().getTime());
            return recordRepository.save(entry);
        }
        else {
            throw new RuntimeException("User not found");
        }
    }

    @PostMapping("/records/remove")
    public void deleteRecord(@RequestParam Integer id) {
        recordRepository.deleteById(id);
    }

    @PostMapping("/user/update")
    public void registerUser(@RequestBody UpdateUserDto userDto) {
        User user = userRepository.getReferenceById(userDto.getUserId());
        user.setBirthday(userDto.getUser().getBirthday());
        user.setEmail(userDto.getUser().getEmail());
        user.setMedicalId(userDto.getUser().getMedicalId());
        user.setGestationDate(userDto.getUser().getGestationDate());
        user.setLogin(userDto.getUser().getLogin());
        user.setPassword(userDto.getUser().getPassword());
        userRepository.save(user);
    }

    @PostMapping("/user")
    public User getUser(@RequestParam Integer id) {
        return userRepository.getReferenceById(id);
    }

    @PostMapping("/user/login")
    public User userLogin(@RequestBody UserLoginDto userLoginDto) {
        User user = userRepository.findByLogin(userLoginDto.getLogin());
        if (user != null) {
            if (!user.getPassword().equals(userLoginDto.getPassword())) {
                throw new RuntimeException("wrong password");
            }
            return user;
        }
        throw new RuntimeException("user not found");
    }

    @PostMapping("/user/registration")
    public Integer registerUser(@RequestBody UserDto userDto) {
        User user = new User();
        user.setBirthday(userDto.getBirthday());
        user.setEmail(userDto.getEmail());
        user.setMedicalId(userDto.getMedicalId());
        user.setGestationDate(userDto.getGestationDate());
        user.setLogin(userDto.getLogin());
        user.setPassword(userDto.getPassword());
        return userRepository.save(user).getId();
    }

}
